# Who am I
> I am Frederic Guilbault, a professional freelancer. My highest interests are online productivity tools and websites. As I value privacy and freedom, Open-source and secure solutions are always favored. I first studied and worked as a industrial mechanic. Fiddling around with coding in my free time, slowly linux and web became obsessive passions.After taking a variety of night&weekend classes and training, in 2013 I decided to dropmy job and focus on sysadmin, PHP and surroundings. I live in Quebec(CA), so yes, french is my mother tongue. I do this by passion and I volunteer as I can (when it's not the sailing season).

<hr/>

# Contact
*Email* <br>
&nbsp;&nbsp;&nbsp;&nbsp;![My email](./email.png)<br>
&nbsp;&nbsp;&nbsp;&nbsp;Fingerprint: 19DF 106B 4A07 F8DA 750A CA24 9BCD DBDB 2F29 FD95<br>
&nbsp;&nbsp;&nbsp;&nbsp;PGP public key : [pgp.asc](./pgp.asc)<br>

*IRC*<br>
&nbsp;&nbsp;&nbsp;&nbsp;If IRC is still your ting, you might find me on freenode.<br>
&nbsp;&nbsp;&nbsp;&nbsp;My nickName is border0464.

*Facebook*<br>
&nbsp;&nbsp;&nbsp;&nbsp;[frederic.m.guilbault](https://www.facebook.com/frederic.m.guilbault)

*Contribution platforms*<br>
&nbsp;&nbsp;&nbsp;&nbsp;Github : [FredericGuilbault](https://github.com/FredericGuilbault)<br>
&nbsp;&nbsp;&nbsp;&nbsp;Gitlab : [FredericGuilbault](https://gitlab.com/FredericGuilbault)<br>
&nbsp;&nbsp;&nbsp;&nbsp;Loomio : @fredericguilbault

<br><br>

<hr>

# My FOSS project

Lysmarine

Gerboise

Loco-linux

# My landscape

| Operating Systems | Languages & frameworks | Productivity | Services |
| --- | --- | --- | --- |
| ArchLinux  | PHP        | PhpStorm   | Apache |
| Debian     | Javascript | Virtualbox | Nginx |
| Linux Mint | Bash       | Filezilla  | Mysql |
| Tails      | CSS        | ssh        | Sqlite |
| Slitaz     | HTML5      | kanboard   | Drupal |
|            | PHPcrawl   | Keepass    | Wordpress |
|            | PHPMailer  | Lastpass   | |
|            | Bootstrap  | Nextcloud  | |
|            | jQuery     | Git        | |
|            | OpenLayers | Mercurial  | |
|            |            | Redmine    | |





# Activities


# Portefolio
